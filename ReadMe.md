# 🚚 DriveAndDeliver - a Carrefour kata

## Overview
Drive And Deliver is a microservice developed in Spring 3.0.0 with Java 21, designed using the hexagonal architecture pattern. This microservice facilitates the selection of delivery methods and scheduling for customers. It provides services through a RESTful API.

## Project Structure
The project follows a hexagonal architecture, separating business logic from delivery mechanisms. It consists of the following components:
- **Domain Layer**:   This layer contains the application's business entities, domain rules and business services. It is completely isolated from infrastructure details.
- **Infrastructure Layer**: This layer acts as a coordination layer between the domain and the adapters. It contains your application's use cases, Adapters: (Input adapters: Manage interaction with clients (e.g. REST controllers in a Spring Boot application) and Output adapters: manage interaction with external resources (e.g. Spring Data JPA repositories)) , which orchestrate business operations using domain entities and services.

## Architecture Schema
```
src
└───com.carrefour
    └───delivery
        ├───domain
        │   ├───annotation
        │   │   └───... Contains custom annotation for domain services
        │   ├───model
        │   │   └───... Contains business model objects
        │   ├───port
        │   │   ├───api
        │   │   │   └───... ports for REST API 
        │   │   └───repository
        │   │       └───... ports for repositories
        │   └───service
        │       └───... Api Ports implementation
        └───infrastructure
            ├───configuration
            	├───... contains list of configurations
            ├───in
            │   ├───adapter
            │   │   └───... incoming adapters using domain Api ports
            │   ├───annotation
            		└───... contains custom Annotations for adapter  
            │   ├───controller
            │   │   └───... contains the REST controllers
            │   ├───dto
            │   │   └───... DTOs & Model for the presenter
            │   ├───exception
            │   │   └───... custom Exceptions
            │   └───mapper
            │       └───... list of mappers
            └───out
                └───annotation
                    └───... custom annotations for persistence Adapters 
                └───persistence
                    └───jpa
                        ├───adapter
                        │   └───... outgoing adapters implementing domain repository ports
                        ├───entity
                        │   └───... JPA entities
                        ├───mapper
                        │   └───... mappers from JPA entities to domain objects layer
                        └───repository
                            └───... JPA repositories

```

## Database Versioning
[Liquibase](https://www.liquibase.org/) is used for database versioning. It helps manage and apply database schema changes as code.

Run the project: 
```
# Run Maven package command
./mvnw clean package