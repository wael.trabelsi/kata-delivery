package com.carrefour.delivery.infrastructure.out.persistence.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.UserEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.UserJpaRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UserJpaRepositoryTest {
	@Autowired
	private UserJpaRepository userJpaRepository;

	@Test
	public void givenUserEntityId_whenFindById_thenReturnUserEntity() {

		// Given: A delivery method is stored in the database
		UserEntity userEntity = UserEntity.builder().firstName("User 1").lastName("User 1").email("user1@email.com")
				.password("password").build();

		userEntity = userJpaRepository.save(userEntity);
		Long userId = userEntity.getId();

		// When: Retrieving delivery method by ID
		Optional<UserEntity> optionalDeliveryMethod = userJpaRepository.findById(userId);

		// Then: The delivery method should be found
		assertNotNull(optionalDeliveryMethod);
		UserEntity foundDeliveryMethod = optionalDeliveryMethod.orElse(null);
		assertNotNull(foundDeliveryMethod);
		assertEquals(userId, foundDeliveryMethod.getId());
	}

}
