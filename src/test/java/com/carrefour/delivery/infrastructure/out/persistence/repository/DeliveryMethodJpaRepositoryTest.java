package com.carrefour.delivery.infrastructure.out.persistence.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethodEnum;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.DeliveryMethodEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.TimeSlotEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.DeliveryMethodJpaRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DeliveryMethodJpaRepositoryTest {

	@Autowired
	private DeliveryMethodJpaRepository deliveryMethodJpaRepository;

	@Test
	public void givenDeliveryMethodEntityList_whenFindAll_thenReturnDeliveryMethodEntityList() {

		// Given: Some delivery methods are stored in the database
		DeliveryMethodEntity deliveryMethod1 = DeliveryMethodEntity.builder().id(1L).name(DeliveryMethodEnum.DRIVE)
				.timeSlotsEntity(new ArrayList<TimeSlotEntity>()).description("Delivery method for drive-in").build();

		DeliveryMethodEntity deliveryMethod2 = DeliveryMethodEntity.builder().id(2L).name(DeliveryMethodEnum.DELIVERY)
				.timeSlotsEntity(new ArrayList<TimeSlotEntity>()).description("Standard delivery method").build();

		DeliveryMethodEntity deliveryMethod3 = DeliveryMethodEntity.builder().id(3L).name(DeliveryMethodEnum.DELIVERY_TODAY)
				.timeSlotsEntity(new ArrayList<TimeSlotEntity>()).description("Delivery Today method").build();
		
		DeliveryMethodEntity deliveryMethod4 = DeliveryMethodEntity.builder().id(4L).name(DeliveryMethodEnum.DELIVERY_ASAP)
				.timeSlotsEntity(new ArrayList<TimeSlotEntity>()).description("Delivery ASAP method").build();
		
		
		deliveryMethodJpaRepository.save(deliveryMethod1);
		deliveryMethodJpaRepository.save(deliveryMethod2);
		deliveryMethodJpaRepository.save(deliveryMethod3);
		deliveryMethodJpaRepository.save(deliveryMethod4);

		// When: Retrieving all delivery methods
		List<DeliveryMethodEntity> deliveryMethodEntities = deliveryMethodJpaRepository.findAll();

		// Then : Verify the output
		Assertions.assertThat(deliveryMethodEntities).isNotEmpty();
		Assertions.assertThat(deliveryMethodEntities.size()).isEqualTo(4);
	}

	@Test
	public void givenDeliveryMethodEntityId_whenFindById_thenReturnDeliveryMethodEntity() {

		// Given: A delivery method is stored in the database
		DeliveryMethodEntity deliveryMethod = DeliveryMethodEntity.builder().id(1L).name(DeliveryMethodEnum.DRIVE)
				.description("Delivery method for drive-in").build();

		deliveryMethod = deliveryMethodJpaRepository.save(deliveryMethod);
		Long deliveryMethodId = deliveryMethod.getId();

		// When: Retrieving delivery method by ID
		Optional<DeliveryMethodEntity> optionalDeliveryMethod = deliveryMethodJpaRepository.findById(deliveryMethodId);

		// Then: The delivery method should be found
		assertNotNull(optionalDeliveryMethod);
		DeliveryMethodEntity foundDeliveryMethod = optionalDeliveryMethod.orElse(null);
		assertNotNull(foundDeliveryMethod);
		assertEquals(deliveryMethodId, foundDeliveryMethod.getId());
	}

}
