package com.carrefour.delivery.infrastructure.out.persistence.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethodEnum;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.DeliveryMethodEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.TimeSlotEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.UserEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.DeliveryMethodJpaRepository;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.TimeSlotJpaRepository;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.UserJpaRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class TimeSlotRepository {
	@Autowired
	private DeliveryMethodJpaRepository deliveryMethodJpaRepository;
	@Autowired
	private TimeSlotJpaRepository timeSlotJpaRepository;
	@Autowired
	private UserJpaRepository userJpaRepository;

	@Test
	public void givenDeliveryMethodEntityId_whenFindByDeliveryMethodId_thenReturnTimeSlotEntityList() {

		// Given: Some TimeSlotEntity are stored in the database
		DeliveryMethodEntity deliveryMethod1 = DeliveryMethodEntity.builder().id(1L).name(DeliveryMethodEnum.DRIVE)
				.description("Delivery method for drive-in").build();

		DeliveryMethodEntity deliveryMethod2 = DeliveryMethodEntity.builder().id(2L).name(DeliveryMethodEnum.DELIVERY)
				.description("Standard delivery method").build();

		DeliveryMethodEntity deliveryMethod3 = DeliveryMethodEntity.builder().id(3L).name(DeliveryMethodEnum.DELIVERY_TODAY)
				.description("Delivery Today method").build();
		
		DeliveryMethodEntity deliveryMethod4 = DeliveryMethodEntity.builder().id(4L).name(DeliveryMethodEnum.DELIVERY_ASAP)
				.description("Delivery ASAP method").build();

		assertNotNull(deliveryMethod1);

		UserEntity userEntity1 = UserEntity.builder().firstName("user 1").lastName("user 1").password("pass")
				.email("user1@email.com").build();

		userEntity1 = userJpaRepository.save(userEntity1);

		ZonedDateTime now = ZonedDateTime.now();

		TimeSlotEntity timeSlotEntity1 = TimeSlotEntity.builder().deliveryMethodEntity(deliveryMethod1)
				.startTime(now).endTime(now.plusHours(1)).userEntity(userEntity1).build();

		TimeSlotEntity timeSlotEntity2 = TimeSlotEntity.builder().deliveryMethodEntity(deliveryMethod1)
				.startTime(now.plusHours(2)).endTime(now.plusHours(3)).userEntity(userEntity1).build();

		List<TimeSlotEntity> timeSlots = Stream.of(timeSlotEntity1, timeSlotEntity2).collect(Collectors.toList());
		deliveryMethod1.setTimeSlotsEntity(timeSlots);

		deliveryMethod1 = deliveryMethodJpaRepository.save(deliveryMethod1);
		Long deliveryMethodId = deliveryMethod1.getId(); 

		// When: Retrieving delivery method by ID
		List<TimeSlotEntity> slots = timeSlotJpaRepository.findBydeliveryMethodEntityId(deliveryMethodId);

		// Then: The delivery method should be found
		Assertions.assertThat(slots).isNotEmpty();
		Assertions.assertThat(slots.size()).isEqualTo(2);
	}
}
