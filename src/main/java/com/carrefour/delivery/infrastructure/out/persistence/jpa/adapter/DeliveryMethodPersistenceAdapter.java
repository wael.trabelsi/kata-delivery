package com.carrefour.delivery.infrastructure.out.persistence.jpa.adapter;

import java.util.List;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethod;
import com.carrefour.delivery.domain.port.repository.DeliveryMethodRepositoryPort;
import com.carrefour.delivery.infrastructure.out.annotation.AdapterPersistence;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.mapper.DeliveryMethodPersistenceMapper;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.DeliveryMethodJpaRepository;

import lombok.RequiredArgsConstructor;

@AdapterPersistence
@RequiredArgsConstructor
public class DeliveryMethodPersistenceAdapter implements DeliveryMethodRepositoryPort {
	private DeliveryMethodJpaRepository deliveryMethodJpaRepository;
	private DeliveryMethodPersistenceMapper deliveryMethodPersistenceMapper;
	
	@Override
	public List<DeliveryMethod> getAllDeliveryMethods() {
		return deliveryMethodPersistenceMapper.toDomainList(deliveryMethodJpaRepository.findAll());
	}
	
	
}
