package com.carrefour.delivery.infrastructure.out.persistence.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.TimeSlotEntity;

@Repository
public interface TimeSlotJpaRepository extends JpaRepository<TimeSlotEntity, Long> {
	List<TimeSlotEntity> findBydeliveryMethodEntityId(Long deliveryMethodId);	
}
