package com.carrefour.delivery.infrastructure.out.persistence.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.DeliveryMethodEntity;

@Repository
public interface DeliveryMethodJpaRepository extends JpaRepository<DeliveryMethodEntity, Long> {

}
