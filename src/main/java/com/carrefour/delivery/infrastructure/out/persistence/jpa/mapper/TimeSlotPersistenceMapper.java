package com.carrefour.delivery.infrastructure.out.persistence.jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.carrefour.delivery.domain.model.delivery.TimeSlot;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.TimeSlotEntity;

@Mapper(componentModel = "spring")
public interface TimeSlotPersistenceMapper {
	/**
	 * Maps a TimeSlotEntity object to a TimeSlot Business object.
	 * @param timeSlotEntity: TimeSlotEntity
	 * @return TimeSlot object
	 */
	TimeSlot toDomain(TimeSlotEntity timeSlotEntity);
	
	/**
	 * Maps a List of timeSlotEntity object to a List of TimeSlot Business object.
	 * @param timeSlotEntities : List<TimeSlotEntity>
	 * @return List<TimeSlot>
	 */
	List<TimeSlot> toDomainList(List<TimeSlotEntity> timeSlotEntities);
}
