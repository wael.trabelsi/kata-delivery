package com.carrefour.delivery.infrastructure.out.persistence.jpa.adapter;

import java.time.ZonedDateTime;
import java.util.List;

import com.carrefour.delivery.domain.model.delivery.TimeSlot;
import com.carrefour.delivery.domain.port.repository.TimeSlotRepositoryPort;
import com.carrefour.delivery.infrastructure.in.exception.DeliveryMethodNotFoundException;
import com.carrefour.delivery.infrastructure.in.exception.UserNotFoundException;
import com.carrefour.delivery.infrastructure.out.annotation.AdapterPersistence;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.DeliveryMethodEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.TimeSlotEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.UserEntity;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.mapper.TimeSlotPersistenceMapper;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.DeliveryMethodJpaRepository;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.TimeSlotJpaRepository;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.repository.UserJpaRepository;

import lombok.RequiredArgsConstructor;

@AdapterPersistence
@RequiredArgsConstructor
public class TimeSlotPersistenceAdapter implements TimeSlotRepositoryPort {
	private final TimeSlotJpaRepository timeSlotJpaRepository;
	private final DeliveryMethodJpaRepository deliveryMethodJpaRepository;
	private final UserJpaRepository userJpaRepository;
	private final TimeSlotPersistenceMapper timeSlotPersistenceMapper;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TimeSlot> getBookedTimeSlotsByDeliveryMethodId(Long id) {
		List<TimeSlotEntity> timeSlotsEntity = timeSlotJpaRepository.findBydeliveryMethodEntityId(id);
		if (timeSlotsEntity != null) {
			return timeSlotPersistenceMapper.toDomainList(timeSlotsEntity);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TimeSlot bookTimeSlotForDeliveryMethod(Long deliveryMethodId, ZonedDateTime startDateTime,
			ZonedDateTime endDateTime, Long userId) {

		DeliveryMethodEntity deliveryMethodEntity = deliveryMethodJpaRepository.findById(deliveryMethodId)
				.orElseThrow(() -> new DeliveryMethodNotFoundException(deliveryMethodId));

		UserEntity userEntity = userJpaRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

		TimeSlotEntity timeSlotEntity = TimeSlotEntity.builder().startTime(startDateTime).endTime(endDateTime)
				.deliveryMethodEntity(deliveryMethodEntity).userEntity(userEntity).build();

		timeSlotEntity = timeSlotJpaRepository.saveAndFlush(timeSlotEntity);
		return timeSlotPersistenceMapper.toDomain(timeSlotEntity);
	}

}
