package com.carrefour.delivery.infrastructure.out.persistence.jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethod;
import com.carrefour.delivery.infrastructure.out.persistence.jpa.entity.DeliveryMethodEntity;

@Mapper(componentModel = "spring")
public interface DeliveryMethodPersistenceMapper {
	/**
	 * Maps a DeliveryMethodEntity object to a DeliveryMethod Business object.
	 * @param DeliveryMethodEntity
	 * @return
	 */
	DeliveryMethod toDomain(DeliveryMethodEntity deliveryMethodEntity);
	/**
	 * Maps a DeliveryMethodEntity object to a DeliveryMethod Business object.
	 * @param deliveryMethodEntities: List<DeliveryMethodEntity>
	 * @return List<DeliveryMethod>
	 */
	List<DeliveryMethod> toDomainList(List<DeliveryMethodEntity> deliveryMethodEntities);
}
