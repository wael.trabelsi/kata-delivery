package com.carrefour.delivery.infrastructure.out.persistence.jpa.entity;

import java.time.ZonedDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "time_slots")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class TimeSlotEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	@Column(name = "start_time", columnDefinition = "TIMESTAMP WITH TIME ZONE")
	private ZonedDateTime startTime;
	@Column(name = "end_time", columnDefinition = "TIMESTAMP WITH TIME ZONE")
	private ZonedDateTime endTime;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "delivery_method_id")
	private DeliveryMethodEntity deliveryMethodEntity;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserEntity userEntity;
}
