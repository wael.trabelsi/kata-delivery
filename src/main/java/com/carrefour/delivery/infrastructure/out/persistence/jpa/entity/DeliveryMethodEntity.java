package com.carrefour.delivery.infrastructure.out.persistence.jpa.entity;

import java.util.ArrayList;
import java.util.List;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethodEnum;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "delivery_methods")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class DeliveryMethodEntity {
	@Id
	private Long id;
	@Enumerated(EnumType.STRING)
	private DeliveryMethodEnum name;
	@Column(name="description")
	private String description;
	@OneToMany(mappedBy = "deliveryMethodEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TimeSlotEntity> timeSlotsEntity = new ArrayList<>();
}
