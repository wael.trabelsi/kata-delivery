package com.carrefour.delivery.infrastructure.in.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carrefour.delivery.infrastructure.in.adapter.TimeSlotAdapter;
import com.carrefour.delivery.infrastructure.in.dto.TimeSlotDto;
import com.carrefour.delivery.infrastructure.in.dto.TimeSlotReservationRequest;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/time-slots")
public class TimeSlotController {
	private final TimeSlotAdapter timeSlotAdapter;

	@GetMapping("/{delivery-method-id}")
	public ResponseEntity<List<TimeSlotDto>> getBookedTimeSlotsByDeliveryMethodId(
			@PathVariable("delivery-method-id") Long deliveryMethodId) {
		return ResponseEntity.ok(timeSlotAdapter.getBookedTimeSlotsByDeliveryMethodId(deliveryMethodId));
	}

	@PostMapping
	public ResponseEntity<String> reserveTimeSlot(@RequestBody TimeSlotReservationRequest reservation) {
		try {
			TimeSlotDto timeSlotDto = timeSlotAdapter.reserveTimeSlot(reservation.getDeliveryMethodId(),
					reservation.getStartDate(), reservation.getEndDate(), reservation.getUserId());
			if (timeSlotDto == null) {
				throw new Exception("Failed to reserve time slot.");
			}
			return ResponseEntity.ok("Time slot reserved successfully.");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed to reserve time slot.");
		}
	}
}
