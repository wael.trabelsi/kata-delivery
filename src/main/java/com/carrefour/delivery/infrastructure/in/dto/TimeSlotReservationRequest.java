package com.carrefour.delivery.infrastructure.in.dto;

import java.time.ZonedDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TimeSlotReservationRequest {
	private Long deliveryMethodId;
    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private Long userId;
}
