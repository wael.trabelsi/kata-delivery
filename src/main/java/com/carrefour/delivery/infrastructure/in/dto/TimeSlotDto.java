package com.carrefour.delivery.infrastructure.in.dto;

import java.time.ZonedDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TimeSlotDto {
	private Long id;
	private ZonedDateTime endTime;
	private ZonedDateTime startTime;
	private Long deliveryMethodId;
	private Long userId;
}
