package com.carrefour.delivery.infrastructure.in.adapter;

import java.util.List;

import com.carrefour.delivery.domain.port.api.DeliveryMethodApiPort;
import com.carrefour.delivery.infrastructure.in.annotation.AdapterApi;
import com.carrefour.delivery.infrastructure.in.dto.DeliveryMethodDto;
import com.carrefour.delivery.infrastructure.in.mapper.DeliveryMethodMapper;

import lombok.RequiredArgsConstructor;

@AdapterApi
@RequiredArgsConstructor
public class DeliveryMethodAdapter {
	private final DeliveryMethodApiPort deliveryMethodApiPort;
	private final DeliveryMethodMapper deliveryMethodMapper;

	public List<DeliveryMethodDto> getAllDeliveryMethods() {
		return deliveryMethodMapper.toDtoList(deliveryMethodApiPort.getAllDeliveryMethods());
	}
}
