package com.carrefour.delivery.infrastructure.in.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carrefour.delivery.infrastructure.in.adapter.DeliveryMethodAdapter;
import com.carrefour.delivery.infrastructure.in.dto.DeliveryMethodDto;
import com.carrefour.delivery.infrastructure.in.exception.DeliveryMethodNotFoundException;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/delivery-methods")
public class DeliveryMethodController {

	private final DeliveryMethodAdapter deliveryMethodAdapter;

	@GetMapping
	public ResponseEntity<List<DeliveryMethodDto>> getAllDeliveryMethods() {
		List<DeliveryMethodDto> deliveryMethods = deliveryMethodAdapter.getAllDeliveryMethods();
		if (deliveryMethods == null || deliveryMethods.isEmpty()) {
			throw new DeliveryMethodNotFoundException();
		}
		return ResponseEntity.ok(deliveryMethods);
	}
}