package com.carrefour.delivery.infrastructure.in.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethod;
import com.carrefour.delivery.infrastructure.in.dto.DeliveryMethodDto;

@Mapper(componentModel = "spring")
public interface DeliveryMethodMapper {
	DeliveryMethodDto toDto(DeliveryMethod deliveryMethod);
	List<DeliveryMethodDto> toDtoList(List<DeliveryMethod> deliveryMethods);
}