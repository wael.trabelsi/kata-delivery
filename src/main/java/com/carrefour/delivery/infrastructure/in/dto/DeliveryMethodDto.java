package com.carrefour.delivery.infrastructure.in.dto;

import java.util.List;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethodEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryMethodDto {
	private Long id;
    private DeliveryMethodEnum name;
    private String description;
    private List<TimeSlotDto> timeSlots;
}
