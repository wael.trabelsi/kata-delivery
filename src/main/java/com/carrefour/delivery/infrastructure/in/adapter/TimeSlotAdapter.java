package com.carrefour.delivery.infrastructure.in.adapter;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.carrefour.delivery.domain.port.api.TimeSlotApiPort;
import com.carrefour.delivery.infrastructure.in.annotation.AdapterApi;
import com.carrefour.delivery.infrastructure.in.dto.TimeSlotDto;
import com.carrefour.delivery.infrastructure.in.mapper.TimeSlotMapper;

import lombok.RequiredArgsConstructor;

@AdapterApi
@RequiredArgsConstructor
public class TimeSlotAdapter {
	private final TimeSlotApiPort timeSlotApiPort;
	private final TimeSlotMapper timeSlotMapper;

	public List<TimeSlotDto> getBookedTimeSlotsByDeliveryMethodId(Long deliveryMethodId) {
		return timeSlotMapper.toDtoList(timeSlotApiPort.getBookedTimeSlotsByDeliveryMethodId(deliveryMethodId));
	}

	public TimeSlotDto reserveTimeSlot(Long deliveryMethodId, ZonedDateTime startDateTime, ZonedDateTime endDateTime,
			Long userId) {

		return timeSlotMapper.toDto(
				timeSlotApiPort.bookTimeSlotForDeliveryMethod(deliveryMethodId, startDateTime, endDateTime, userId));
	}
}
