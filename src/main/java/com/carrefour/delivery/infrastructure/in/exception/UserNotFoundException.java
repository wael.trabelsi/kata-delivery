package com.carrefour.delivery.infrastructure.in.exception;

public class UserNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3043634405385166725L;

	public UserNotFoundException(Long id) {
		super(String.format("The User id: %d is not found.", id));
	}
}