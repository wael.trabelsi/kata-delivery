package com.carrefour.delivery.infrastructure.in.exception;

public class DeliveryMethodNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4569285237429208379L;

	public DeliveryMethodNotFoundException(Long id) {
		super(String.format("The Delivery method id: %d is not found.", id));
	}

	public DeliveryMethodNotFoundException() {
		super("No Delivery method found!");
	}
}