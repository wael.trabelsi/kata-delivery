package com.carrefour.delivery.infrastructure.in.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.carrefour.delivery.domain.model.delivery.TimeSlot;
import com.carrefour.delivery.infrastructure.in.dto.TimeSlotDto;

@Mapper(componentModel = "spring")
public interface TimeSlotMapper {
	TimeSlotDto toDto(TimeSlot timeSlot);
	List<TimeSlotDto> toDtoList(List<TimeSlot> timeSlots);
}
