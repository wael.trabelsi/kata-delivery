package com.carrefour.delivery.infrastructure.in.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {

	@ExceptionHandler(DeliveryMethodNotFoundException.class)
	public ResponseEntity<Object> handleDeliveryMethodNotFoundException(DeliveryMethodNotFoundException ex) {
		log.debug("Handling DeliveryMethodNotFoundException: {}", ex.getMessage());
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex) {
		log.debug("Handling UserNotFoundException: {}", ex.getMessage());
		return ResponseEntity.notFound().build();
	}
}
