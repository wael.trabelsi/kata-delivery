package com.carrefour.delivery.domain.service;

import java.time.ZonedDateTime;
import java.util.List;

import com.carrefour.delivery.domain.annotation.DomainService;
import com.carrefour.delivery.domain.model.delivery.TimeSlot;
import com.carrefour.delivery.domain.port.api.TimeSlotApiPort;
import com.carrefour.delivery.domain.port.repository.TimeSlotRepositoryPort;

import lombok.RequiredArgsConstructor;

@DomainService
@RequiredArgsConstructor
public class TimeSlotService implements TimeSlotApiPort {
	private final TimeSlotRepositoryPort timeSlotRepositoryPort;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TimeSlot> getBookedTimeSlotsByDeliveryMethodId(Long id) {
		return timeSlotRepositoryPort.getBookedTimeSlotsByDeliveryMethodId(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TimeSlot bookTimeSlotForDeliveryMethod(Long deliveryMethodId, ZonedDateTime startDateTime,
			ZonedDateTime endDateTime, Long userId) {
		return timeSlotRepositoryPort.bookTimeSlotForDeliveryMethod(deliveryMethodId, startDateTime, endDateTime,
				userId);
	}
}
