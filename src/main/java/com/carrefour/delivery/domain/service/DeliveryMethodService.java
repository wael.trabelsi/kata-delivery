package com.carrefour.delivery.domain.service;

import java.util.List;

import com.carrefour.delivery.domain.annotation.DomainService;
import com.carrefour.delivery.domain.model.delivery.DeliveryMethod;
import com.carrefour.delivery.domain.port.api.DeliveryMethodApiPort;
import com.carrefour.delivery.domain.port.repository.DeliveryMethodRepositoryPort;

import lombok.RequiredArgsConstructor;

@DomainService
@RequiredArgsConstructor
public class DeliveryMethodService implements DeliveryMethodApiPort{
	
	private final DeliveryMethodRepositoryPort deliveryMethodRepositoryPort;

	@Override
	public List<DeliveryMethod> getAllDeliveryMethods() {
		return deliveryMethodRepositoryPort.getAllDeliveryMethods();
	}


}
