package com.carrefour.delivery.domain.model.delivery;

public enum DeliveryMethodEnum {
	DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
