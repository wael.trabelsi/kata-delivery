package com.carrefour.delivery.domain.model.delivery;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryMethod {
    private Long id;
    private DeliveryMethodEnum name;
    private String description;
    private List<TimeSlot> timeSlots;
}
