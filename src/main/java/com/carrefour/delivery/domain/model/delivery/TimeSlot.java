package com.carrefour.delivery.domain.model.delivery;

import java.time.ZonedDateTime;

import com.carrefour.delivery.domain.model.user.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TimeSlot {
    private Long id;
    private ZonedDateTime endTime;
    private ZonedDateTime startTime;
    private DeliveryMethod deliveryMethod;
    private User user;
}
