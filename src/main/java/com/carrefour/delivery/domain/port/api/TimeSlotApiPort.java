package com.carrefour.delivery.domain.port.api;

import java.time.ZonedDateTime;
import java.util.List;

import com.carrefour.delivery.domain.model.delivery.TimeSlot;

public interface TimeSlotApiPort {

	/**
	 * Retrieve all available TimeSlots based on the selected delivery method
	 * 
	 * @param id : The delivery method id
	 * @return List of booked TimeSlot
	 */
	List<TimeSlot> getBookedTimeSlotsByDeliveryMethodId(Long id);

	/**
	 * Create a TimeSlot based on delivery method, the selected timeSlot and the
	 * user
	 * 
	 * @param deliveryMethodId: The delivery method id
	 * @param startDate:        the startDate
	 * @param endDate:          the endDate
	 * @param userId:           the User id
	 * @return: created TimeSlot
	 */
	TimeSlot bookTimeSlotForDeliveryMethod(Long deliveryMethodId, ZonedDateTime startDateTime, ZonedDateTime endDateTime,
			Long userId);
}
