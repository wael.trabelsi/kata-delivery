package com.carrefour.delivery.domain.port.repository;

import java.util.List;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethod;


public interface DeliveryMethodRepositoryPort {
	
	List<DeliveryMethod> getAllDeliveryMethods();
}
