package com.carrefour.delivery.domain.port.repository;

import java.time.ZonedDateTime;
import java.util.List;

import com.carrefour.delivery.domain.model.delivery.TimeSlot;


public interface TimeSlotRepositoryPort {

	/**
	 * Retrieve all available TimeSlots based on the selected delivery method
	 * 
	 * @param id : The delivery method id
	 * @return List of booked TimeSlot
	 */
	List<TimeSlot> getBookedTimeSlotsByDeliveryMethodId(Long id);

	/**
	 * Create a TimeSlot
	 * 
	 * @param deliveryMethodId: The deliveryMethodId
	 * @param startDateTime:        start dateTime
	 * @param endDateTime:          end DateTime
	 * @param userId:            The user Id
	 * @return the created TimeSlot
	 */
	TimeSlot bookTimeSlotForDeliveryMethod(Long deliveryMethodId, ZonedDateTime startDateTime, ZonedDateTime endDateTime,
			Long userId);

}
