package com.carrefour.delivery.domain.port.api;

import java.util.List;

import com.carrefour.delivery.domain.model.delivery.DeliveryMethod;

public interface DeliveryMethodApiPort {
	/**
	 * Get all available delivery methods
	 * 
	 * @return List of DeliveryMethod
	 */
	List<DeliveryMethod> getAllDeliveryMethods();
}
